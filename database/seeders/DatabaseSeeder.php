<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $foods_data = [
            [
                "order_date" => "01/01/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 33,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 58.41
            ],
            [
                "order_date" => "01/04/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 87,
                "unitPrice" => 3.49,
                "totalPrice" => 303.63
            ],
            [
                "order_date" => "01/07/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 58,
                "unitPrice" => 1.87,
                "totalPrice" => 108.46
            ],
            [
                "order_date" => "01/10/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 82,
                "unitPrice" => 1.87,
                "totalPrice" => 153.34
            ],
            [
                "order_date" => "01/13/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 38,
                "unitPrice" => 2.18,
                "totalPrice" => 82.84
            ],
            [
                "order_date" => "01/16/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 54,
                "unitPrice" => 1.77,
                "totalPrice" => 95.58
            ],
            [
                "order_date" => "01/19/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 149,
                "unitPrice" => 3.49,
                "totalPrice" => 520.01
            ],
            [
                "order_date" => "01/22/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 51,
                "unitPrice" => 1.77,
                "totalPrice" => 90.27
            ],
            [
                "order_date" => "01/25/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 100,
                "unitPrice" => 1.77,
                "totalPrice" => 177
            ],
            [
                "order_date" => "01/28/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 28,
                "unitPrice" => 1.35,
                "totalPrice" => 37.800000000000004
            ],
            [
                "order_date" => "01/31/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 36,
                "unitPrice" => 2.18,
                "totalPrice" => 78.48
            ],
            [
                "order_date" => "02/03/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 31,
                "unitPrice" => 1.87,
                "totalPrice" => 57.97
            ],
            [
                "order_date" => "02/06/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 28,
                "unitPrice" => 3.49,
                "totalPrice" => 97.72
            ],
            [
                "order_date" => "02/09/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 44,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 77.88
            ],
            [
                "order_date" => "02/12/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 23,
                "unitPrice" => 1.77,
                "totalPrice" => 40.71
            ],
            [
                "order_date" => "02/15/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 27,
                "unitPrice" => 1.35,
                "totalPrice" => 36.45
            ],
            [
                "order_date" => "02/18/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 43,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 93.73999999999998
            ],
            [
                "order_date" => "02/21/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 123,
                "unitPrice" => 2.84,
                "totalPrice" => 349.32
            ],
            [
                "order_date" => "02/24/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 42,
                "unitPrice" => 1.87,
                "totalPrice" => 78.54
            ],
            [
                "order_date" => "02/27/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 33,
                "unitPrice" => 2.84,
                "totalPrice" => 93.72
            ],
            [
                "order_date" => "03/02/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 85,
                "unitPrice" => 1.87,
                "totalPrice" => 158.95
            ],
            [
                "order_date" => "03/05/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 30,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 85.2
            ],
            [
                "order_date" => "03/08/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 61,
                "unitPrice" => 1.77,
                "totalPrice" => 107.97
            ],
            [
                "order_date" => "03/11/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 40,
                "unitPrice" => 3.49,
                "totalPrice" => 139.6
            ],
            [
                "order_date" => "03/14/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 86,
                "unitPrice" => 1.87,
                "totalPrice" => 160.82
            ],
            [
                "order_date" => "03/17/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 38,
                "unitPrice" => 1.7700000000000002,
                "totalPrice" => 67.26
            ],
            [
                "order_date" => "03/20/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 68,
                "unitPrice" => 1.68,
                "totalPrice" => 114.24
            ],
            [
                "order_date" => "03/23/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 39,
                "unitPrice" => 1.87,
                "totalPrice" => 72.93
            ],
            [
                "order_date" => "03/26/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 103,
                "unitPrice" => 1.87,
                "totalPrice" => 192.61
            ],
            [
                "order_date" => "03/29/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 193,
                "unitPrice" => 2.84,
                "totalPrice" => 548.12
            ],
            [
                "order_date" => "04/01/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 58,
                "unitPrice" => 1.77,
                "totalPrice" => 102.66
            ],
            [
                "order_date" => "04/04/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 68,
                "unitPrice" => 1.68,
                "totalPrice" => 114.24
            ],
            [
                "order_date" => "04/07/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 91,
                "unitPrice" => 1.77,
                "totalPrice" => 161.07
            ],
            [
                "order_date" => "04/10/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 23,
                "unitPrice" => 3.49,
                "totalPrice" => 80.27
            ],
            [
                "order_date" => "04/13/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 28,
                "unitPrice" => 1.68,
                "totalPrice" => 47.04
            ],
            [
                "order_date" => "04/16/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 48,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 84.96
            ],
            [
                "order_date" => "04/19/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 134,
                "unitPrice" => 1.68,
                "totalPrice" => 225.12
            ],
            [
                "order_date" => "04/22/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 20,
                "unitPrice" => 1.77,
                "totalPrice" => 35.4
            ],
            [
                "order_date" => "04/25/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 53,
                "unitPrice" => 1.77,
                "totalPrice" => 93.81
            ],
            [
                "order_date" => "04/28/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 64,
                "unitPrice" => 1.68,
                "totalPrice" => 107.52
            ],
            [
                "order_date" => "05/01/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 63,
                "unitPrice" => 1.87,
                "totalPrice" => 117.81
            ],
            [
                "order_date" => "05/04/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 105,
                "unitPrice" => 1.87,
                "totalPrice" => 196.35
            ],
            [
                "order_date" => "05/07/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 138,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 391.92
            ],
            [
                "order_date" => "05/10/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 25,
                "unitPrice" => 1.77,
                "totalPrice" => 44.25
            ],
            [
                "order_date" => "05/13/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 21,
                "unitPrice" => 3.49,
                "totalPrice" => 73.29
            ],
            [
                "order_date" => "05/16/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 61,
                "unitPrice" => 1.77,
                "totalPrice" => 107.97
            ],
            [
                "order_date" => "05/19/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 49,
                "unitPrice" => 1.68,
                "totalPrice" => 82.32
            ],
            [
                "order_date" => "05/22/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 55,
                "unitPrice" => 1.87,
                "totalPrice" => 102.85
            ],
            [
                "order_date" => "05/25/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 27,
                "unitPrice" => 2.18,
                "totalPrice" => 58.86000000000001
            ],
            [
                "order_date" => "05/28/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 58,
                "unitPrice" => 1.77,
                "totalPrice" => 102.66
            ],
            [
                "order_date" => "05/31/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 33,
                "unitPrice" => 3.49,
                "totalPrice" => 115.17
            ],
            [
                "order_date" => "06/03/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 288,
                "unitPrice" => 2.84,
                "totalPrice" => 817.92
            ],
            [
                "order_date" => "06/06/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 76,
                "unitPrice" => 1.87,
                "totalPrice" => 142.12
            ],
            [
                "order_date" => "06/09/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 42,
                "unitPrice" => 1.77,
                "totalPrice" => 74.34
            ],
            [
                "order_date" => "06/12/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 20,
                "unitPrice" => 3.49,
                "totalPrice" => 69.8
            ],
            [
                "order_date" => "06/15/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 75,
                "unitPrice" => 1.77,
                "totalPrice" => 132.75
            ],
            [
                "order_date" => "06/18/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 38,
                "unitPrice" => 3.49,
                "totalPrice" => 132.62
            ],
            [
                "order_date" => "06/21/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 306,
                "unitPrice" => 1.77,
                "totalPrice" => 541.62
            ],
            [
                "order_date" => "06/24/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 28,
                "unitPrice" => 1.68,
                "totalPrice" => 47.04
            ],
            [
                "order_date" => "06/27/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 110,
                "unitPrice" => 1.87,
                "totalPrice" => 205.7
            ],
            [
                "order_date" => "06/30/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 51,
                "unitPrice" => 2.84,
                "totalPrice" => 144.84
            ],
            [
                "order_date" => "07/03/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 52,
                "unitPrice" => 1.77,
                "totalPrice" => 92.04
            ],
            [
                "order_date" => "07/06/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 28,
                "unitPrice" => 3.49,
                "totalPrice" => 97.72
            ],
            [
                "order_date" => "07/09/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 136,
                "unitPrice" => 1.77,
                "totalPrice" => 240.72
            ],
            [
                "order_date" => "07/12/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 42,
                "unitPrice" => 3.49,
                "totalPrice" => 146.58
            ],
            [
                "order_date" => "07/15/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 75,
                "unitPrice" => 1.87,
                "totalPrice" => 140.25
            ],
            [
                "order_date" => "07/18/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 72,
                "unitPrice" => 1.87,
                "totalPrice" => 134.64
            ],
            [
                "order_date" => "07/21/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 56,
                "unitPrice" => 2.84,
                "totalPrice" => 159.04
            ],
            [
                "order_date" => "07/24/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 51,
                "unitPrice" => 1.87,
                "totalPrice" => 95.37
            ],
            [
                "order_date" => "07/27/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 31,
                "unitPrice" => 1.68,
                "totalPrice" => 52.08
            ],
            [
                "order_date" => "07/30/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 56,
                "unitPrice" => 1.87,
                "totalPrice" => 104.72
            ],
            [
                "order_date" => "08/02/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 137,
                "unitPrice" => 2.84,
                "totalPrice" => 389.08
            ],
            [
                "order_date" => "08/05/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 107,
                "unitPrice" => 1.87,
                "totalPrice" => 200.09
            ],
            [
                "order_date" => "08/08/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 24,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 42.48
            ],
            [
                "order_date" => "08/11/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 30,
                "unitPrice" => 3.49,
                "totalPrice" => 104.7
            ],
            [
                "order_date" => "08/14/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 70,
                "unitPrice" => 1.87,
                "totalPrice" => 130.9
            ],
            [
                "order_date" => "08/17/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 31,
                "unitPrice" => 2.18,
                "totalPrice" => 67.58
            ],
            [
                "order_date" => "08/20/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 109,
                "unitPrice" => 1.77,
                "totalPrice" => 192.93
            ],
            [
                "order_date" => "08/23/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 21,
                "unitPrice" => 3.49,
                "totalPrice" => 73.29
            ],
            [
                "order_date" => "08/26/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 80,
                "unitPrice" => 1.87,
                "totalPrice" => 149.6
            ],
            [
                "order_date" => "08/29/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 75,
                "unitPrice" => 1.87,
                "totalPrice" => 140.25
            ],
            [
                "order_date" => "09/01/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 74,
                "unitPrice" => 2.84,
                "totalPrice" => 210.16
            ],
            [
                "order_date" => "09/04/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 45,
                "unitPrice" => 1.77,
                "totalPrice" => 79.65
            ],
            [
                "order_date" => "09/07/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 28,
                "unitPrice" => 2.18,
                "totalPrice" => 61.040000000000006
            ],
            [
                "order_date" => "09/10/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 143,
                "unitPrice" => 1.77,
                "totalPrice" => 253.11
            ],
            [
                "order_date" => "09/13/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 27,
                "unitPrice" => 3.15,
                "totalPrice" => 85.05
            ],
            [
                "order_date" => "09/16/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 133,
                "unitPrice" => 1.77,
                "totalPrice" => 235.41
            ],
            [
                "order_date" => "09/19/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 110,
                "unitPrice" => 2.18,
                "totalPrice" => 239.8
            ],
            [
                "order_date" => "09/22/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 65,
                "unitPrice" => 1.87,
                "totalPrice" => 121.55
            ],
            [
                "order_date" => "09/25/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 33,
                "unitPrice" => 1.87,
                "totalPrice" => 61.71
            ],
            [
                "order_date" => "09/28/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 81,
                "unitPrice" => 2.18,
                "totalPrice" => 176.58
            ],
            [
                "order_date" => "10/01/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 77,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 136.29
            ],
            [
                "order_date" => "10/04/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 38,
                "unitPrice" => 3.49,
                "totalPrice" => 132.62
            ],
            [
                "order_date" => "10/07/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 40,
                "unitPrice" => 1.77,
                "totalPrice" => 70.8
            ],
            [
                "order_date" => "10/10/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 114,
                "unitPrice" => 1.6800000000000002,
                "totalPrice" => 191.52
            ],
            [
                "order_date" => "10/13/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 224,
                "unitPrice" => 2.18,
                "totalPrice" => 488.32000000000005
            ],
            [
                "order_date" => "10/16/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 141,
                "unitPrice" => 1.77,
                "totalPrice" => 249.57
            ],
            [
                "order_date" => "10/19/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 32,
                "unitPrice" => 3.49,
                "totalPrice" => 111.68
            ],
            [
                "order_date" => "10/22/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 20,
                "unitPrice" => 1.77,
                "totalPrice" => 35.4
            ],
            [
                "order_date" => "10/25/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 40,
                "unitPrice" => 2.18,
                "totalPrice" => 87.2
            ],
            [
                "order_date" => "10/28/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 49,
                "unitPrice" => 1.87,
                "totalPrice" => 91.63
            ],
            [
                "order_date" => "10/31/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 46,
                "unitPrice" => 3.49,
                "totalPrice" => 160.54
            ],
            [
                "order_date" => "11/03/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 39,
                "unitPrice" => 1.77,
                "totalPrice" => 69.03
            ],
            [
                "order_date" => "11/06/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 62,
                "unitPrice" => 1.68,
                "totalPrice" => 104.16
            ],
            [
                "order_date" => "11/09/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 90,
                "unitPrice" => 1.77,
                "totalPrice" => 159.3
            ],
            [
                "order_date" => "11/12/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 103,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 224.53999999999996
            ],
            [
                "order_date" => "11/15/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 32,
                "unitPrice" => 2.84,
                "totalPrice" => 90.88
            ],
            [
                "order_date" => "11/18/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 66,
                "unitPrice" => 1.87,
                "totalPrice" => 123.42
            ],
            [
                "order_date" => "11/21/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 97,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 275.48
            ],
            [
                "order_date" => "11/24/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 30,
                "unitPrice" => 1.77,
                "totalPrice" => 53.1
            ],
            [
                "order_date" => "11/27/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 29,
                "unitPrice" => 1.68,
                "totalPrice" => 48.72
            ],
            [
                "order_date" => "11/30/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 92,
                "unitPrice" => 1.77,
                "totalPrice" => 162.84
            ],
            [
                "order_date" => "12/03/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 139,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 303.02
            ],
            [
                "order_date" => "12/06/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 29,
                "unitPrice" => 2.84,
                "totalPrice" => 82.36
            ],
            [
                "order_date" => "12/09/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Banana",
                "quantity" => 30,
                "unitPrice" => 2.27,
                "totalPrice" => 68.1
            ],
            [
                "order_date" => "12/12/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 36,
                "unitPrice" => 1.87,
                "totalPrice" => 67.32
            ],
            [
                "order_date" => "12/15/2020",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 41,
                "unitPrice" => 3.49,
                "totalPrice" => 143.09
            ],
            [
                "order_date" => "12/18/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 44,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 77.88
            ],
            [
                "order_date" => "12/21/2020",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 29,
                "unitPrice" => 1.68,
                "totalPrice" => 48.72
            ],
            [
                "order_date" => "12/24/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 237,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 516.66
            ],
            [
                "order_date" => "12/27/2020",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 65,
                "unitPrice" => 1.87,
                "totalPrice" => 121.55
            ],
            [
                "order_date" => "12/30/2020",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 83,
                "unitPrice" => 2.18,
                "totalPrice" => 180.94000000000003
            ],
            [
                "order_date" => "01/02/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 32,
                "unitPrice" => 2.18,
                "totalPrice" => 69.76
            ],
            [
                "order_date" => "01/05/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 63,
                "unitPrice" => 1.77,
                "totalPrice" => 111.51
            ],
            [
                "order_date" => "01/08/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 29,
                "unitPrice" => 3.15,
                "totalPrice" => 91.35
            ],
            [
                "order_date" => "01/11/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 77,
                "unitPrice" => 1.87,
                "totalPrice" => 143.99
            ],
            [
                "order_date" => "01/14/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 80,
                "unitPrice" => 2.84,
                "totalPrice" => 227.2
            ],
            [
                "order_date" => "01/17/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 102,
                "unitPrice" => 1.77,
                "totalPrice" => 180.54
            ],
            [
                "order_date" => "01/20/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 31,
                "unitPrice" => 3.49,
                "totalPrice" => 108.19
            ],
            [
                "order_date" => "01/23/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 56,
                "unitPrice" => 1.77,
                "totalPrice" => 99.12
            ],
            [
                "order_date" => "01/26/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 52,
                "unitPrice" => 2.18,
                "totalPrice" => 113.36000000000001
            ],
            [
                "order_date" => "01/29/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 51,
                "unitPrice" => 1.77,
                "totalPrice" => 90.27
            ],
            [
                "order_date" => "02/01/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 24,
                "unitPrice" => 1.68,
                "totalPrice" => 40.32
            ],
            [
                "order_date" => "02/04/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 58,
                "unitPrice" => 2.18,
                "totalPrice" => 126.44000000000001
            ],
            [
                "order_date" => "02/07/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 34,
                "unitPrice" => 1.87,
                "totalPrice" => 63.58
            ],
            [
                "order_date" => "02/10/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 34,
                "unitPrice" => 1.77,
                "totalPrice" => 60.18
            ],
            [
                "order_date" => "02/13/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 21,
                "unitPrice" => 1.6800000000000002,
                "totalPrice" => 35.28
            ],
            [
                "order_date" => "02/16/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 29,
                "unitPrice" => 2.84,
                "totalPrice" => 82.36
            ],
            [
                "order_date" => "02/19/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 68,
                "unitPrice" => 1.77,
                "totalPrice" => 120.36
            ],
            [
                "order_date" => "02/22/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 31,
                "unitPrice" => 3.1500000000000004,
                "totalPrice" => 97.65
            ],
            [
                "order_date" => "02/25/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 30,
                "unitPrice" => 2.18,
                "totalPrice" => 65.4
            ],
            [
                "order_date" => "02/28/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 232,
                "unitPrice" => 1.87,
                "totalPrice" => 433.84
            ],
            [
                "order_date" => "03/02/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 68,
                "unitPrice" => 1.87,
                "totalPrice" => 127.16
            ],
            [
                "order_date" => "03/05/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 97,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 275.48
            ],
            [
                "order_date" => "03/08/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 86,
                "unitPrice" => 1.87,
                "totalPrice" => 160.82
            ],
            [
                "order_date" => "03/11/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 41,
                "unitPrice" => 1.68,
                "totalPrice" => 68.88
            ],
            [
                "order_date" => "03/14/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 93,
                "unitPrice" => 1.7700000000000002,
                "totalPrice" => 164.61
            ],
            [
                "order_date" => "03/17/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 47,
                "unitPrice" => 1.68,
                "totalPrice" => 78.96
            ],
            [
                "order_date" => "03/20/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 103,
                "unitPrice" => 1.77,
                "totalPrice" => 182.31
            ],
            [
                "order_date" => "03/23/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 33,
                "unitPrice" => 1.68,
                "totalPrice" => 55.44
            ],
            [
                "order_date" => "03/26/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 57,
                "unitPrice" => 1.87,
                "totalPrice" => 106.59
            ],
            [
                "order_date" => "03/29/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 65,
                "unitPrice" => 2.84,
                "totalPrice" => 184.6
            ],
            [
                "order_date" => "04/01/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 118,
                "unitPrice" => 1.77,
                "totalPrice" => 208.86
            ],
            [
                "order_date" => "04/04/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 36,
                "unitPrice" => 2.18,
                "totalPrice" => 78.48
            ],
            [
                "order_date" => "04/07/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 123,
                "unitPrice" => 2.84,
                "totalPrice" => 349.32
            ],
            [
                "order_date" => "04/10/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 90,
                "unitPrice" => 1.77,
                "totalPrice" => 159.3
            ],
            [
                "order_date" => "04/13/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 21,
                "unitPrice" => 3.49,
                "totalPrice" => 73.29
            ],
            [
                "order_date" => "04/16/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 48,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 84.96
            ],
            [
                "order_date" => "04/19/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 24,
                "unitPrice" => 1.68,
                "totalPrice" => 40.32
            ],
            [
                "order_date" => "04/22/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 67,
                "unitPrice" => 1.87,
                "totalPrice" => 125.29
            ],
            [
                "order_date" => "04/25/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 27,
                "unitPrice" => 1.87,
                "totalPrice" => 50.49
            ],
            [
                "order_date" => "04/28/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 129,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 366.36
            ],
            [
                "order_date" => "05/01/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 77,
                "unitPrice" => 2.18,
                "totalPrice" => 167.86
            ],
            [
                "order_date" => "05/04/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 58,
                "unitPrice" => 1.87,
                "totalPrice" => 108.46
            ],
            [
                "order_date" => "05/07/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 47,
                "unitPrice" => 1.87,
                "totalPrice" => 87.89
            ],
            [
                "order_date" => "05/10/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 33,
                "unitPrice" => 2.84,
                "totalPrice" => 93.72
            ],
            [
                "order_date" => "05/13/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 82,
                "unitPrice" => 1.87,
                "totalPrice" => 153.34
            ],
            [
                "order_date" => "05/16/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 58,
                "unitPrice" => 1.77,
                "totalPrice" => 102.66
            ],
            [
                "order_date" => "05/19/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 30,
                "unitPrice" => 3.15,
                "totalPrice" => 94.5
            ],
            [
                "order_date" => "05/22/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 43,
                "unitPrice" => 1.87,
                "totalPrice" => 80.41
            ],
            [
                "order_date" => "05/25/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 84,
                "unitPrice" => 1.77,
                "totalPrice" => 148.68
            ],
            [
                "order_date" => "05/28/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 36,
                "unitPrice" => 2.18,
                "totalPrice" => 78.48
            ],
            [
                "order_date" => "05/31/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 44,
                "unitPrice" => 2.84,
                "totalPrice" => 124.96
            ],
            [
                "order_date" => "06/03/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 27,
                "unitPrice" => 1.87,
                "totalPrice" => 50.49
            ],
            [
                "order_date" => "06/06/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 120,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 340.8
            ],
            [
                "order_date" => "06/09/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 26,
                "unitPrice" => 3.49,
                "totalPrice" => 90.74
            ],
            [
                "order_date" => "06/12/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 73,
                "unitPrice" => 1.77,
                "totalPrice" => 129.21
            ],
            [
                "order_date" => "06/15/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 38,
                "unitPrice" => 1.87,
                "totalPrice" => 71.06
            ],
            [
                "order_date" => "06/18/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 40,
                "unitPrice" => 2.84,
                "totalPrice" => 113.6
            ],
            [
                "order_date" => "06/21/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 41,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 72.57
            ],
            [
                "order_date" => "06/24/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Banana",
                "quantity" => 27,
                "unitPrice" => 2.27,
                "totalPrice" => 61.29
            ],
            [
                "order_date" => "06/27/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 38,
                "unitPrice" => 1.87,
                "totalPrice" => 71.06
            ],
            [
                "order_date" => "06/30/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 34,
                "unitPrice" => 3.49,
                "totalPrice" => 118.66
            ],
            [
                "order_date" => "07/03/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 65,
                "unitPrice" => 1.87,
                "totalPrice" => 121.55
            ],
            [
                "order_date" => "07/06/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 60,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 170.4
            ],
            [
                "order_date" => "07/09/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 37,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 80.66
            ],
            [
                "order_date" => "07/12/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 40,
                "unitPrice" => 1.87,
                "totalPrice" => 74.8
            ],
            [
                "order_date" => "07/15/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 26,
                "unitPrice" => 1.87,
                "totalPrice" => 48.62
            ],
            [
                "order_date" => "07/18/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Banana",
                "quantity" => 22,
                "unitPrice" => 2.27,
                "totalPrice" => 49.94
            ],
            [
                "order_date" => "07/21/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 32,
                "unitPrice" => 1.87,
                "totalPrice" => 59.84
            ],
            [
                "order_date" => "07/24/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 23,
                "unitPrice" => 3.49,
                "totalPrice" => 80.27
            ],
            [
                "order_date" => "07/27/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 20,
                "unitPrice" => 2.18,
                "totalPrice" => 43.6
            ],
            [
                "order_date" => "07/30/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 64,
                "unitPrice" => 1.87,
                "totalPrice" => 119.68
            ],
            [
                "order_date" => "08/02/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 71,
                "unitPrice" => 1.77,
                "totalPrice" => 125.67
            ],
            [
                "order_date" => "08/05/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 90,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 196.2
            ],
            [
                "order_date" => "08/08/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 38,
                "unitPrice" => 2.84,
                "totalPrice" => 107.91999999999999
            ],
            [
                "order_date" => "08/11/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 55,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 97.35
            ],
            [
                "order_date" => "08/14/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 22,
                "unitPrice" => 3.15,
                "totalPrice" => 69.3
            ],
            [
                "order_date" => "08/17/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 34,
                "unitPrice" => 1.77,
                "totalPrice" => 60.18
            ],
            [
                "order_date" => "08/20/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 39,
                "unitPrice" => 1.87,
                "totalPrice" => 72.93
            ],
            [
                "order_date" => "08/23/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 41,
                "unitPrice" => 2.84,
                "totalPrice" => 116.44
            ],
            [
                "order_date" => "08/26/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 41,
                "unitPrice" => 1.7699999999999998,
                "totalPrice" => 72.57
            ],
            [
                "order_date" => "08/29/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 136,
                "unitPrice" => 2.18,
                "totalPrice" => 296.48
            ],
            [
                "order_date" => "09/01/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 25,
                "unitPrice" => 1.77,
                "totalPrice" => 44.25
            ],
            [
                "order_date" => "09/04/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 26,
                "unitPrice" => 3.1500000000000004,
                "totalPrice" => 81.9
            ],
            [
                "order_date" => "09/07/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 50,
                "unitPrice" => 1.87,
                "totalPrice" => 93.5
            ],
            [
                "order_date" => "09/10/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 79,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 224.36
            ],
            [
                "order_date" => "09/13/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 30,
                "unitPrice" => 1.77,
                "totalPrice" => 53.1
            ],
            [
                "order_date" => "09/16/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 20,
                "unitPrice" => 1.6800000000000002,
                "totalPrice" => 33.6
            ],
            [
                "order_date" => "09/19/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 49,
                "unitPrice" => 1.77,
                "totalPrice" => 86.73
            ],
            [
                "order_date" => "09/22/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 40,
                "unitPrice" => 2.18,
                "totalPrice" => 87.2
            ],
            [
                "order_date" => "09/25/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 31,
                "unitPrice" => 1.77,
                "totalPrice" => 54.87
            ],
            [
                "order_date" => "09/28/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Snacks",
                "product" => "Pretzels",
                "quantity" => 21,
                "unitPrice" => 3.1500000000000004,
                "totalPrice" => 66.15
            ],
            [
                "order_date" => "10/01/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 43,
                "unitPrice" => 1.87,
                "totalPrice" => 80.41
            ],
            [
                "order_date" => "10/04/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 47,
                "unitPrice" => 2.84,
                "totalPrice" => 133.48
            ],
            [
                "order_date" => "10/07/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 175,
                "unitPrice" => 2.18,
                "totalPrice" => 381.5
            ],
            [
                "order_date" => "10/10/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 23,
                "unitPrice" => 1.87,
                "totalPrice" => 43.01
            ],
            [
                "order_date" => "10/13/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 40,
                "unitPrice" => 1.77,
                "totalPrice" => 70.8
            ],
            [
                "order_date" => "10/16/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 87,
                "unitPrice" => 2.18,
                "totalPrice" => 189.66000000000003
            ],
            [
                "order_date" => "10/19/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 43,
                "unitPrice" => 1.77,
                "totalPrice" => 76.11
            ],
            [
                "order_date" => "10/22/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 30,
                "unitPrice" => 3.49,
                "totalPrice" => 104.7
            ],
            [
                "order_date" => "10/25/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 35,
                "unitPrice" => 1.77,
                "totalPrice" => 61.95
            ],
            [
                "order_date" => "10/28/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 57,
                "unitPrice" => 1.87,
                "totalPrice" => 106.59
            ],
            [
                "order_date" => "10/31/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Snacks",
                "product" => "Potato Chips",
                "quantity" => 25,
                "unitPrice" => 1.68,
                "totalPrice" => 42
            ],
            [
                "order_date" => "11/03/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 24,
                "unitPrice" => 1.87,
                "totalPrice" => 44.88
            ],
            [
                "order_date" => "11/06/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 83,
                "unitPrice" => 1.87,
                "totalPrice" => 155.21
            ],
            [
                "order_date" => "11/09/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 124,
                "unitPrice" => 2.8400000000000003,
                "totalPrice" => 352.16
            ],
            [
                "order_date" => "11/12/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 137,
                "unitPrice" => 1.77,
                "totalPrice" => 242.49
            ],
            [
                "order_date" => "11/15/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 146,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 318.28
            ],
            [
                "order_date" => "11/18/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 34,
                "unitPrice" => 1.87,
                "totalPrice" => 63.58
            ],
            [
                "order_date" => "11/21/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 20,
                "unitPrice" => 1.77,
                "totalPrice" => 35.4
            ],
            [
                "order_date" => "11/24/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 139,
                "unitPrice" => 2.1799999999999997,
                "totalPrice" => 303.02
            ],
            [
                "order_date" => "11/27/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 211,
                "unitPrice" => 1.87,
                "totalPrice" => 394.57
            ],
            [
                "order_date" => "11/30/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 20,
                "unitPrice" => 3.49,
                "totalPrice" => 69.8
            ],
            [
                "order_date" => "12/03/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 42,
                "unitPrice" => 1.87,
                "totalPrice" => 78.54
            ],
            [
                "order_date" => "12/06/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 100,
                "unitPrice" => 2.84,
                "totalPrice" => 284
            ],
            [
                "order_date" => "12/09/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Bars",
                "product" => "Carrot",
                "quantity" => 38,
                "unitPrice" => 1.7700000000000002,
                "totalPrice" => 67.26
            ],
            [
                "order_date" => "12/12/2021",
                "region" => "East",
                "city" => "New York",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 25,
                "unitPrice" => 3.49,
                "totalPrice" => 87.25
            ],
            [
                "order_date" => "12/15/2021",
                "region" => "West",
                "city" => "San Diego",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 96,
                "unitPrice" => 1.87,
                "totalPrice" => 179.52
            ],
            [
                "order_date" => "12/18/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Arrowroot",
                "quantity" => 34,
                "unitPrice" => 2.18,
                "totalPrice" => 74.12
            ],
            [
                "order_date" => "12/21/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Cookies",
                "product" => "Chocolate Chip",
                "quantity" => 245,
                "unitPrice" => 1.87,
                "totalPrice" => 458.15
            ],
            [
                "order_date" => "12/24/2021",
                "region" => "East",
                "city" => "Boston",
                "category" => "Crackers",
                "product" => "Whole Wheat",
                "quantity" => 30,
                "unitPrice" => 3.49,
                "totalPrice" => 104.7
            ],
            [
                "order_date" => "12/27/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Bars",
                "product" => "Bran",
                "quantity" => 30,
                "unitPrice" => 1.87,
                "totalPrice" => 56.1
            ],
            [
                "order_date" => "12/30/2021",
                "region" => "West",
                "city" => "Los Angeles",
                "category" => "Cookies",
                "product" => "Oatmeal Raisin",
                "quantity" => 44,
                "unitPrice" => 2.84,
                "totalPrice" => 124.96
            ]
        ];
        DB::table('foods')->insert($foods_data);
    }
}
