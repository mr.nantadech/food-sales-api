<?php

namespace App\Http\Controllers\api;

use App\Models\Foods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $food_data = Foods::orderBy("id", "asc")->get();
        return response()->json($food_data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            Foods::create($request->all());
            return response()->json([
                'message' => "On Create data success",

            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => "On Create data error",
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $food_data = Foods::find($id);
        return response()->json($food_data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $food_data = Foods::find($id);
        try {
            $food_data->update($request->all());
            return response()->json([
                'message' => "On Update data success",
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => "On Update data error",

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $food_data = Foods::find($id);
        try {
            $food_data::destroy($id);
            return response()->json([
                'message' => "On Delete data success",
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => "On Delete data error",
            ]);
        }
    }

    public function getCategory()
    {
        $food_data_category = DB::table('foods')
            ->select('category')
            ->groupBy('category')
            ->get();
        return response()->json($food_data_category);
    }
}
