<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foods extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'foods';
    protected $fillable = ['order_date', 'region', 'city', 'category', 'product', 'quantity', 'unitprice', 'totalprice'];
}
